'use strict';

const axios = require("axios");

module.exports.proxy = async (event) => {
  const url = event['queryStringParameters']['url'];

  if (!url) {
    return {
      statusCode: 400,
      body: 'URL is missing or not valid',
    }
  }

  try {
    const result = await axios.get(url);

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/rss+xml'
      },
      body: result.data,
    };
  } catch (e) {
    return {
      statusCode: 500,
      body: e.message
    }
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
